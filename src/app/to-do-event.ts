export class ToDoEvent {
    work:string;
    value:number;
    isDone:boolean;
    constructor(currentWork:string,currentValue:number,currentDone:boolean){
        this.work=currentWork;
        this.value=currentValue;
        this.isDone=currentDone;
    }
}
