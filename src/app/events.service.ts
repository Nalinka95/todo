import { Injectable } from '@angular/core';
import { ToDoEvent } from './to-do-event';


@Injectable()
export class EventsService {
  private listEvents:ToDoEvent[]=[{work:"Reaad a novel",value:1,isDone:false}, {work:"Watch T10",value:2,isDone:false},{work: "Check the account",value:3,isDone:false}];
  constructor() { }
  getEvents():ToDoEvent[]{
    return this.listEvents;
  }
  setEvent(currentWork:string,currentValue:number,currentDone:boolean){
    this.listEvents.push(new ToDoEvent(currentWork,currentValue,currentDone));
  }
  deleteEventByValue(val:number){
    this.listEvents = this.listEvents.filter(item => item.value !== val);
  } 

}
