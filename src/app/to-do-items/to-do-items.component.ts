import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';

@Component({
  selector: 'app-to-do-items',
  templateUrl: './to-do-items.component.html',
  styleUrls: ['./to-do-items.component.css']
})
export class ToDoItemsComponent implements OnInit {

  @Input() public doItem;
  @Output() value=new EventEmitter();
  @Output() isWorkCompleted=new EventEmitter();
  
  constructor() { }

  ngOnInit() {
  }

  deleteItem(e){
    this.value.emit(e);
  }

  changeCompletion(e){
    this.isWorkCompleted.emit(e);
    
  }
}
