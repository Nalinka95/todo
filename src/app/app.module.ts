import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { ToDoListComponent } from './to-do-list/to-do-list.component';
import { ToDoItemsComponent } from './to-do-items/to-do-items.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EventsService } from './events.service';


@NgModule({
  declarations: [
    AppComponent,
    ToDoListComponent,
    ToDoItemsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule 
  ],
  providers: [EventsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
