import { Component, OnInit } from '@angular/core';
import { EventsService } from '../events.service';
import { ToDoEvent } from '../to-do-event';

@Component({
  selector: 'app-to-do-list',
  templateUrl: './to-do-list.component.html',
  styleUrls: ['./to-do-list.component.css']
})
export class ToDoListComponent implements OnInit {

  public items =[];
  work:string;
  value:number;
  isDone:boolean;

  check;

  constructor(private _eventService: EventsService) { }

  ngOnInit() {
    this.items=this._eventService.getEvents();
  
  }
  addEvent(event:string){
   this.work= event;
   if(!(this.work=='')){
    this.value=this.generateValue();
    this.isDone=false;
    this.items.push(new ToDoEvent(this.work,this.value,this.isDone));
   }
  }

  changeIsCompleted(completedItem){
    let itemIndex = this.items.findIndex(item => item.value == completedItem.value);
    if(completedItem.isDone){
      this.items[itemIndex]=new ToDoEvent(completedItem.work,completedItem.value,false);
    }
    else{
      this.items[itemIndex]=new ToDoEvent(completedItem.work,completedItem.value,true);
    }
    this.check=this.items[itemIndex].work;
  }
  generateValue():number{
    return Math.floor(Math.random() * (999999 - 100000)) + 100000;
  }
  getValue(e){
    this.value=e;
    this.items = this.items.filter(item => item.value !== this.value);  
  }

}
